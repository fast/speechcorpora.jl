# SPDX-License-Identifier: CECILL-2.1


"""
    abstract type SpeechCorpus end

Abstract type for all speech corpora.
"""
abstract type SpeechCorpus end


"""
    lang(corpus)

Return the ISO 639-3 code of the language of the corpus.
"""
lang


"""
    name(corpus)

Return the name identifier of the corpus.
"""
name


"""
    download(corpus, rootdir)

Download the data of the corpus to `dir`.
"""
Base.download

"""
    prepare(corpus, rootdir)

Prepare the manifests of corpus.
"""
prepare

