# SPDX-License-Identifier: CECILL-2.1

struct SpeechDataset <: MLUtils.AbstractDataContainer
    idxs::Vector{AbstractString}
    annotations::Dict{AbstractString, Annotation}
    recordings::Dict{AbstractString, Recording}
end

"""
dataset(manifestroot)

Load `SpeechDataset` from manifest files stored in `manifestroot`.

Each item of the dataset is a nested tuple `((samples, sampling_rate), Annotation.data)`.

See also [`Annotation`](@ref).

# Examples
```julia-repl
julia> ds = dataset("./manifests", :train)
SpeechDataset(
    ...
)
julia> ds[1]
(
    (samples=[...], sampling_rate=16_000),
    Dict(
        "text" => "Annotation text here"
    )
)
```
"""
function dataset(manifestroot::AbstractString, partition)
    partition_name = partition == "" ? "" : "-$(partition)"
    annot_path =  joinpath(manifestroot, "annotations$(partition_name).jsonl") 
    rec_path = joinpath(manifestroot, "recordings.jsonl")
    annotations = load(Annotation, annot_path)
    recordings = load(Recording, rec_path)
    dataset(annotations, recordings)
end

function dataset(annotations::AbstractDict, recordings::AbstractDict)
    idxs = collect(keys(annotations))
    SpeechDataset(idxs, annotations, recordings)
end

Base.getindex(d::SpeechDataset, key::AbstractString) = d.recordings[key], d.annotations[key]
Base.getindex(d::SpeechDataset, idx::Integer) = getindex(d, d.idxs[idx])
# Fix1 -> partial funcion with fixed 1st argument
Base.getindex(d::SpeechDataset, idxs::AbstractVector) = map(Base.Fix1(getindex, d), idxs)

Base.length(d::SpeechDataset) = length(d.idxs)

function Base.filter(fn, d::SpeechDataset)
    fidxs = filter(d.idxs) do i
        fn((d.recordings[i], d.annotations[i]))
    end
    idset = Set(fidxs)

    fannotations = filter(d.annotations) do (k, v)
        k ∈ idset
    end

    frecs = filter(d.recordings) do (k, v)
        k ∈ idset
    end

    SpeechDataset(fidxs, fannotations, frecs)
end

