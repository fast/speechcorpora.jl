# SpeechDatasets.jl

A Julia package to download and prepare speech corpus.

## Installation

Make sure to add the [FAST registry](https://gitlab.lisn.upsaclay.fr/fast/registry)
to your julia installation. Then, install the package as usual:
```
pkg> add SpeechDatasets
```

## Example

```
julia> using SpeechDatasets

julia> dataset = MINILIBRISPEECH("outputdir", :train) # :dev | :test
...

julia> dataset = TIMIT("/path/to/timit/dir", "outputdir", :train) # :dev | :test
...

julia> dataset = INADIACHRONY("/path/to/ina_wav/dir", "outputdir", "/path/to/ina_csv/dir") # ina_csv dir optional
...

julia> dataset = AVID("/path/to/avid/dir", "outputdir")
...

julia> dataset = SPEECH2TEX("/path/to/speech2tex/dir", "outputdir")
...


julia> for ((signal, fs), supervision) in dataset
           # do something
       end

# Lexicons
julia> CMUDICT("outputfile")
...

julia> TIMITDICT("/path/to/timit/dir")
...

```

## License

This software is provided under the CeCILL 2.1 license (see the [`/LICENSE`](/LICENSE))

